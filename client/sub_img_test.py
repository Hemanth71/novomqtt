import base64
import cv2 as cv
import numpy as np
import paho.mqtt.client as mqtt

MQTT_BROKER = "127.0.0.1"
MQTT_RECEIVE1 = "tess/depth"
MQTT_RECEIVE2 = "tess/rgb"

frame1 = np.zeros((240, 320, 3), np.uint8)
frame2 = np.zeros((240, 320, 3), np.uint8)

# The callback for when the client receives a CONNACK response from the server.
def on_connect1(client1, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client1.subscribe(MQTT_RECEIVE1)

def on_connect2(client2, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client2.subscribe(MQTT_RECEIVE2)

# The callback for when a PUBLISH message is received from the server.
def on_message1(client1, userdata1, msg):
    global frame1
    # Decoding the message
    img1 = base64.b64decode(msg.payload)
    # converting into numpy array from buffer
    npimg1 = np.frombuffer(img1, dtype=np.uint8)
    # Decode to Original Frame
    frame1 = cv.imdecode(npimg1, 1)

def on_message2(client2, userdata2, msg):
    global frame2
    # Decoding the message
    img2 = base64.b64decode(msg.payload)
    # converting into numpy array from buffer
    npimg2 = np.frombuffer(img2, dtype=np.uint8)
    # Decode to Original Frame
    frame2 = cv.imdecode(npimg2, 1)

client1 = mqtt.Client()
client1.on_connect = on_connect1
client1.on_message = on_message1

client2 = mqtt.Client()
client2.on_connect = on_connect2
client2.on_message = on_message2

client1.connect(MQTT_BROKER)
client2.connect(MQTT_BROKER)

# Starting thread which will receive the frames
client1.loop_start()
client2.loop_start()

while True:
    cv.imshow("Stream1", frame1)
    cv.imshow("Stream2", frame2)
    if cv.waitKey(1) & 0xFF == ord('q'):
        break

# Stop the Thread
client1.loop_stop()
client2.loop_stop()