import paho.mqtt.client as mqtt
import numpy as np
import base64
import cv2 as cv
import open3d as o3d

class NovoReceiver(mqtt.Client):
    MQTT_BROKER = "127.0.0.1"
    port = 1883
    MQTT_TOPICS = ["tess/depth", "tess/rgb"]
    frame_depth = np.zeros((240, 320, 3), np.uint8)
    frame_rgb = np.zeros((240, 320, 3), np.uint8)

    intrinsic_parameter = {}
    intrinsic_parameter["fx"] = 388.946
    intrinsic_parameter["fy"] = 388.946
    intrinsic_parameter["cx"] = 321.504
    intrinsic_parameter["cy"] = 237.973
    min = 0
    max = 0


    # vis = o3d.visualization.Visualizer()
    # vis.create_window("o3d Pointcloud")
    # pointcloud = o3d.geometry.PointCloud()
    # geometrie_added = False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # custom initialization

    def on_message(self, client, userdata, message):
        # print("message received ",str(message.payload.decode("utf-8")))
        # print("message received ", message.topic)
        # Decoding the message
        img1 = base64.b64decode(message.payload)
        # converting into numpy array from buffer
        npimg1 = np.frombuffer(img1, dtype=np.uint8)
        if message.topic ==  "tess/depth":
            # self.frame1 = img1
            self.frame_depth = cv.imdecode(npimg1, cv.IMREAD_ANYDEPTH)

        if message.topic ==  "tess/rgb":
            self.frame_rgb = cv.imdecode(npimg1, cv.IMREAD_UNCHANGED)
            
    def on_connect(self, client2, userdata, flags, rc):
        print("connected")

    def make_connection(self):
        self.connect(self.MQTT_BROKER, port=self.port)
        for topic in self.MQTT_TOPICS:
            self.subscribe(topic)
        self.loop_start()
    
    def stop_receiving(self):
        self.loop_stop()
        self.disconnect()

    def generate_pcd(self, filter_dist):
        ## convert depth image to points
        try:
            if self.frame_depth is not None:
                fx = self.intrinsic_parameter["fx"]
                fy = self.intrinsic_parameter["fy"]
                ux = self.intrinsic_parameter["cx"]
                vy = self.intrinsic_parameter["cy"]

                [height, width] = self.frame_depth.shape

                nx = np.linspace(0, width - 1, width)
                ny = np.linspace(0, height - 1, height)

                u, v = np.meshgrid(nx, ny)
                x = (u.flatten() - ux) / fx
                y = (v.flatten() - vy) / fy

                z = self.frame_depth.flatten()

                x = np.multiply(x, z)
                y = np.multiply(y, z)
                x = x[np.nonzero(z)]
                y = y[np.nonzero(z)]
                z = z[np.nonzero(z)]

                xyz = np.zeros((x.shape[0], 3))
                xyz[:, 0] = x
                xyz[:, 1] = y
                xyz[:, 2] = z
                xyz = self.filter_point_cloud_distance(points=xyz, axis_dir="z", val=1000,
                                  keep_lower=True)
                return xyz
        except:
            return None

    def filter_point_cloud_distance(self, points, axis_dir, val, keep_lower=True):
        if keep_lower:
            if axis_dir == "z":
                ids = np.where(points[:, 2] < val)
                points = points[ids]
            elif axis_dir == "y":
                ids = np.where(points[:, 1] < val)
                points = points[ids]
            elif axis_dir == "x":
                ids = np.where(points[:, 0] < val)
                points = points[ids]
        else:
            # Remove all points which are below $val$
            if axis_dir == "z":
                ids = np.where(points[:, 2] > val)
                points = points[ids]
            elif axis_dir == "y":
                ids = np.where(points[:, 1] > val)
                points = points[ids]
            elif axis_dir == "x":
                ids = np.where(points[:, 0] > val)
                points = points[ids]
        return points