from novo_receiver import NovoReceiver
import cv2
import open3d as o3d
import numpy as np

reciever = NovoReceiver()
reciever.MQTT_BROKER = "192.168.1.103"
filter_dist = 2500
reciever.make_connection()

vis = o3d.visualization.Visualizer()
vis.create_window("o3d Pointcloud")
pointcloud = o3d.geometry.PointCloud()
geometrie_added = False

while True:
    depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(reciever.frame_depth, alpha=0.03), cv2.COLORMAP_JET)
    cv2.imshow("Depth_map", depth_colormap)
    # cv2.imshow("RGB frame", reciever.frame_rgb)

    pcd = reciever.generate_pcd(filter_dist)
    # print(pcd.shape)
    if (pcd is not None):
        if isinstance(pcd, np.ndarray):
            # print(pcd.shape)
            pointcloud.points = o3d.utility.Vector3dVector(pcd)
            pointcloud.remove_non_finite_points()
        elif isinstance(pcd, o3d.cpu.pybind.geometry.PointCloud):
            pointcloud.points = pcd.points
    if not geometrie_added:
        vis.add_geometry(pointcloud)
        geometrie_added = True
    vis.update_geometry(pointcloud)
    vis.poll_events()
    vis.update_renderer()

    if cv2.waitKey(1) & 0xFF == ord('q'):
        reciever.stop_receiving()
        break
