# Importing Libraries
import cv2 as cv
import paho.mqtt.client as mqtt
import base64
import time
import random


MQTT_BROKER = '127.0.0.1'
port = 1883
MQTT_SEND1 = "tess/depth"
MQTT_SEND2 = "tess/rgb"


# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 1000)}'
username = 'emqx'
password = 'public'
#host_name = '192.168.179.12'

font = cv.FONT_HERSHEY_SIMPLEX
  
# org
org = (50, 50)
  
# fontScale
fontScale = 1
   
# Blue color in BGR
color = (255, 0, 0)
  
# Line thickness of 2 px
thickness = 2

# Object to capture the frames
cap = cv.VideoCapture(1)
# Phao-MQTT Clinet
client1 = mqtt.Client()
client2 = mqtt.Client()
# Establishing Connection with the Broker
client1.connect(MQTT_BROKER, port=port)
client2.connect(MQTT_BROKER, port=port)
try:
 while True:
  start = time.time()
  # Read Frame
  _, frame = cap.read()
  # Encoding the Frame
  
  #frame = cv.putText(frame, str(time.now()), org, font, 
  #                 fontScale, color, thickness, cv.LINE_AA)
  _, buffer = cv.imencode('.jpg', frame)
  # Converting into encoded bytes
  jpg_as_text = base64.b64encode(buffer)
  # Publishig the Frame on the Topic home/server
  client1.publish(MQTT_SEND1, jpg_as_text)
  client2.publish(MQTT_SEND2, jpg_as_text)
  end = time.time()
  t = end - start
  fps = 1/t
  print(fps)
except:
 cap.release()
 client1.disconnect()
 client2.disconnect()
 print("\nNow you can restart fresh")
