import pyrealsense2 as rs
import numpy as np
import cv2
import random
import paho.mqtt.client as mqtt
import base64
import open3d as o3d

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()

# Get device product line for setting a supporting resolution
pipeline_wrapper = rs.pipeline_wrapper(pipeline)
pipeline_profile = config.resolve(pipeline_wrapper)
device = pipeline_profile.get_device()
device_product_line = str(device.get_info(rs.camera_info.product_line))

found_rgb = False
for s in device.sensors:
    if s.get_info(rs.camera_info.name) == 'RGB Camera':
        found_rgb = True
        break
if not found_rgb:
    print("The demo requires Depth camera with Color sensor")
    exit(0)

## TODO: @Hemanth - find max resolution of Depth image possible
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)

## TODO: @Hemanth - find max resolution of RGB image possible
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

## TODO: @Manan - Define and initialize MQTT client and topics (tess/depth & tess/rgb)
MQTT_BROKER = '127.0.0.1'
port = 1883
topic_depth = "tess/depth" # client1
topic_rgb = "tess/rgb" # client2

# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 1000)}'
username = 'emqx'
password = 'public'

# Start streaming (locally)
pipeline.start(config)

## TODO: @Manan - Start MQTT Clients
client_depth = mqtt.Client() ### l_cam
client_rgb = mqtt.Client() ### r_cam

client_depth.connect(MQTT_BROKER)
client_rgb.connect(MQTT_BROKER)
intrinsic_parameter = {}
intrinsic_parameter["fx"] = 648.243
intrinsic_parameter["fy"] = 648.243
intrinsic_parameter["cx"] = 642.506
intrinsic_parameter["cy"] = 356.622

# vis = o3d.visualization.Visualizer()
# vis.create_window("Test")
# pointcloud = o3d.geometry.PointCloud()
# geometrie_added = False


def realsense_pcd(depth_image, intrinsic_parameters):
    """
    This only works for realsense camera
    """
    # TODO: Do not Filter point cloud here (May need raw point cloud for other operation)
    try:
        if depth_image is not None:
            fx = intrinsic_parameters["fx"]
            fy = intrinsic_parameters["fy"]
            ux = intrinsic_parameters["cx"]
            vy = intrinsic_parameters["cy"]

            [height, width] = depth_image.shape

            nx = np.linspace(0, width - 1, width)
            ny = np.linspace(0, height - 1, height)

            u, v = np.meshgrid(nx, ny)
            x = (u.flatten() - ux) / fx
            y = (v.flatten() - vy) / fy

            z = depth_image.flatten()

            x = np.multiply(x, z)
            y = np.multiply(y, z)
            x = x[np.nonzero(z)]
            y = y[np.nonzero(z)]
            z = z[np.nonzero(z)]

            xyz = np.zeros((x.shape[0], 3))
            xyz[:, 0] = x
            xyz[:, 1] = y
            xyz[:, 2] = z
            return xyz
    except:
        return None

try:
    while True:

        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()
        depth_frame = frames.get_depth_frame()
        color_frame = frames.get_color_frame()
        if not depth_frame or not color_frame:
            continue

        # Convert images to numpy arrays
        depth_image = np.asanyarray(depth_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())
        

        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)
        # print(depth_image.shape)
        # pcd = realsense_pcd(depth_image, intrinsic_parameter)
        # if (pcd is not None):
        #     # print(pcd.shape)
        #     if isinstance(pcd, np.ndarray):
        #         # print('here')
        #         pointcloud.points = o3d.utility.Vector3dVector(pcd)
        #     elif isinstance(pcd, o3d.cpu.pybind.geometry.PointCloud):
        #         pointcloud.points = pcd.points
        # if not geometrie_added:
        #     vis.add_geometry(pointcloud)
        #     geometrie_added = True
        # vis.update_geometry(pointcloud)
        # vis.poll_events()
        # vis.update_renderer()

        # print("Before Max", np.max(depth_image))
        _, buffer = cv2.imencode('.png', depth_image)
        ## TODO: @Manan - add MQTT publisher for the above depth and color arrays
        # print('before', depth_colormap.shape)
        depth_frame = base64.b64encode(buffer)
        client_depth.publish(topic_depth, depth_frame)
        # img1 = cv2.imdecode(buffer, cv2.IMREAD_ANYDEPTH)
        # print("After encode", np.max(img1))
        # print('published 1')


        _, buffer = cv2.imencode('.jpg', color_image)
        # print('beofre rgb ', color_image.shape)
        rgb_frame = base64.b64encode(buffer)
        client_rgb.publish(topic_rgb, rgb_frame)
        # print('published 2')

        # depth_colormap_dim = depth_colormap.shape
        # print(1, depth_colormap_dim)
        # color_colormap_dim = color_image.shape
        # print(2, color_colormap_dim)

        # If depth and color resolutions are different, resize color image to match depth image for display
        # if depth_colormap_dim != color_colormap_dim:
        #     resized_color_image = cv2.resize(color_image, dsize=(depth_colormap_dim[1], depth_colormap_dim[0]), interpolation=cv2.INTER_AREA)
        #     images = np.hstack((resized_color_image, depth_colormap))
        # else:
        #     images = np.hstack((color_image, depth_colormap))

        # Show images
        # cv2.namedWindow('Tess_Konnect', cv2.WINDOW_AUTOSIZE)
        # cv2.imshow('Tess_Konnect DepthMap1', img1)
        # cv2.imshow('Tess_Konnect DepthMap', depth_image)
        # cv2.imshow('Tess_Konnect RGB', color_image)
        # cv2.waitKey(1)

finally:

    # Stop streaming
    pipeline.stop()
    client_depth.disconnect()
    client_rgb.disconnect()

